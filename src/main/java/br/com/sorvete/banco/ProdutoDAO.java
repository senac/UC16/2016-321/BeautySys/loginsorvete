
package br.com.sorvete.banco;

import br.com.sorvete.entity.Produto;


public class ProdutoDAO extends DAO<Produto>{
    
    public ProdutoDAO() {
        super(Produto.class);
    }
    
}
