
package br.com.sorvete.banco;

import br.com.sorvete.entity.Cliente;


public class ClienteDAO extends DAO<Cliente>{
    
    public ClienteDAO() {
        super(Cliente.class);
    }
    
}
