
package br.com.sorvete.banco;

import br.com.sorvete.entity.Usuario;
import javax.persistence.NoResultException;

public class UsuarioDAO extends DAO<Usuario>{
    
    public UsuarioDAO() {
        super(Usuario.class);
    }
}