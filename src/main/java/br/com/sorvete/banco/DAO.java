package br.com.sorvete.banco;

import br.com.sorvete.entity.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public abstract class DAO<T> {

    private EntityManager em;

    private final Class<T> entidade;

    public DAO(Class<T> entidade) {
        this.entidade = entidade;
    }

    public void save(T entity) {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();
        this.em.persist(entity);
        this.em.getTransaction().commit();
        this.em.close();

    }

    public List<T> findAll() {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();

        Query query = em.createQuery("from " + entidade.getName() + " e");
        List<T> lista = query.getResultList();

        this.em.getTransaction().commit();
        this.em.close();

        return lista;

    }

    public void update(T entity) {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();
        this.em.merge(entity);
        this.em.getTransaction().commit();
        this.em.close();
    }

    public void delete(int id) {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();
         T t = em.find(entidade , id);
        this.em.remove(t);
        this.em.getTransaction().commit();
        this.em.close();
    }

    public T find(int id) {
        this.em = JPAUtil.getEntityManager();
        this.em.getTransaction().begin();
        T t = em.find(entidade, id);
        this.em.getTransaction().commit();
        this.em.close();

        return t;
    }
    
    /*Metodos da Classe de Login do Usuário*/ 
    public Usuario getUsuario(String nomeUsuario, String senha) {
   
              try {
                    Usuario usuario = (Usuario) em                       
                            .createQuery("SELECT u from Usuario u where u.nomeUsuario = :name and u.senha = :senha")
                            .setParameter("name", nomeUsuario)
                            .setParameter("senha", senha).getSingleResult();
   
                    return usuario;
              } catch (NoResultException e) {
                    return null;
              }
        }
    
    public boolean inserirUsuario(Usuario usuario) {
              try {
                    em.persist(usuario);
                    return true;
              } catch (Exception e) {
                    e.printStackTrace();
                    return false;
              }
        }
        
    public boolean deletarUsuario(Usuario usuario) {
              try {
                    em.remove(usuario);
                    return true;
              } catch (Exception e) {
                    e.printStackTrace();
                    return false;
              }
        }
   
  }