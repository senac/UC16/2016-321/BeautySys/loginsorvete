
package br.com.sorvete.banco;

import br.com.sorvete.entity.Agenda;

public class AgendaDAO extends DAO<Agenda>{
    
    public AgendaDAO() {
        super(Agenda.class);
    }
    
}
