
package br.com.sorvete.bean;

import br.com.sorvete.banco.AgendaDAO;
import br.com.sorvete.entity.Agenda;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;


@Named(value = "agendaBean")
@ViewScoped
public class AgendaBean extends Bean {

    private Agenda agenda;
    private AgendaDAO dao;

    public AgendaBean() {
    }

    @PostConstruct
    public void init() {
       this.dao = new AgendaDAO();
       this.novo();
    }

    public String getCodigo() {
        return this.agenda.getId() == 0 ? "" : String.valueOf(this.agenda.getId());
    }

    public void novo() {
        this.agenda = new Agenda();
       
    }

    public void salvar() {

        try {

            if (this.agenda.getId() == 0) {
                dao.save(agenda);
                addMessageInfo("Salvo com sucesso!");
                novo();
            } else {
                dao.update(agenda);
                addMessageInfo("Alterado com sucesso!");
                novo();
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }  

    public void excluir(Agenda agenda) {
        try {
            dao.delete(agenda.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    public List<Agenda> getLista() {
        return this.dao.findAll();
    }
   
}
 

