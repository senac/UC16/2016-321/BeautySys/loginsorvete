
package br.com.sorvete.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.*;

@Entity
public class Agenda implements Serializable {
    
    private String descricao;
            
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Temporal(TemporalType.TIME)
    private Date tempoExecucao;
    
    @Temporal(TemporalType.DATE)
    private Date dataExecucao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTempoExecucao() {
        return tempoExecucao;
    }

    public void setTempoExecucao(Date tempoExecucao) {
        this.tempoExecucao = tempoExecucao;
    }

    public Date getDataExecucao() {
        return dataExecucao;
    }

    public void setDataExecucao(Date dataExecucao) {
        this.dataExecucao = dataExecucao;
    }
    
}
